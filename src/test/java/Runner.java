import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

public class Runner {

  public static void main(String[] args) {
    TestNG testNG = new TestNG();

    XmlSuite suite = new XmlSuite();
    List<String> suitesPaths = new ArrayList<>();
    suitesPaths.add("src/test/resources/pastebin-suite.xml");
    suite.setSuiteFiles(suitesPaths);
    List<XmlSuite> suites = new ArrayList<>();
    suites.add(suite);

    testNG.setXmlSuites(suites);
    testNG.run();
  }
}
