package tests;

import modules.builder.PasteBuilder;
import modules.model.PasteBin;
import modules.page.pastebin.PasteBinHomePage;
import modules.page.pastebin.PostedPastePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateNewPasteTest extends BaseTest {

  @Test(description = "Create new paste")
  public void createNewPaste() {
    PasteBinHomePage page = new PasteBinHomePage(driver);
    PasteBin pasteBinModel = new PasteBuilder().buildPaste();
    page.open().createNewPaste(pasteBinModel);
    Assert.assertTrue(page.isSuccessStatus(), "PasteBin is not created!");
  }

  @Test(
      description = "Create bash script paste",
      dependsOnMethods = "createNewPaste",
      alwaysRun = true)
  public void createBashScriptPaste() {
    PasteBin pasteBinModel = new PasteBuilder().buildPasteBash();
    PasteBinHomePage page = new PasteBinHomePage(driver);
    page.open().createNewPaste(pasteBinModel);
    PostedPastePage postedPastePage = new PostedPastePage(driver);
    Assert.assertTrue(page.isSuccessStatus(), "PasteBin is not created!");
    Assert.assertEquals(postedPastePage.titleName(), pasteBinModel.getPasteTitle());
    Assert.assertEquals(postedPastePage.pasteRawCode(), pasteBinModel.getCode());
    Assert.assertTrue(postedPastePage.isCodeBashSyntax(), "Code is not in bash syntax");
  }
}
