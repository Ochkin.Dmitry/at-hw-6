package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

abstract class BaseTest {

  protected WebDriver driver;

  @BeforeClass
  public void setUpTests() {
    driver = new ChromeDriver();
    driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
  }

  @AfterClass
  public void tearDownTests() {
    driver.quit();
  }
}
