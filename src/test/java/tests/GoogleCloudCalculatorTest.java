package tests;

import modules.builder.ComputeEngineBuilder;
import modules.model.ComputeEngine;
import modules.page.google_cloud.ComputeEnginePage;
import modules.page.google_cloud.GoogleCloudPage;
import modules.page.google_cloud.PricingCalculatorPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleCloudCalculatorTest extends BaseTest {

  @Test(description = "Calculate compute engine")
  public void calculateComputeEngine() {
    PricingCalculatorPage calculatorPage =
        new GoogleCloudPage(driver).open().openPricingCalculatorPage();
    ComputeEngine product = new ComputeEngineBuilder().buildComputeEngineForm();
    ComputeEnginePage computeEnginePage = calculatorPage.selectComputeEngineSection();
    computeEnginePage.fillComputeEngineForm(product);
    computeEnginePage.addToEstimateInstances();
    computeEnginePage.verifyComputeEngineEstimateForm();
    Assert.assertTrue(
        computeEnginePage.isExpectedEstimateValue(
            computeEnginePage.getVMClassEstimated(), product.getMachineClass().toLowerCase()));
    Assert.assertTrue(
        computeEnginePage.isExpectedEstimateValue(
            computeEnginePage.getInstanceEstimated(), "n1-standard-8"));
    Assert.assertTrue(
        computeEnginePage.isExpectedEstimateValue(
            computeEnginePage.getRegionEstimated(), "Frankfurt"));
    Assert.assertTrue(
        computeEnginePage.isExpectedEstimateValue(
            computeEnginePage.getLocalSSDSpaceEstimated(), " 2x375 GiB"));
    Assert.assertEquals(computeEnginePage.getEstimateCost(), "USD 1,288.71 per 1 month");
  }
}
