package modules.model;

public class PasteBin {

  private String code;
  private String pasteTitle;
  private String syntaxHighlighting;
  private String pasteExpiration;
  private String pasteExposure;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getPasteTitle() {
    return pasteTitle;
  }

  public void setPasteTitle(String pasteTitle) {
    this.pasteTitle = pasteTitle;
  }

  public String getSyntaxHighlighting() {
    return syntaxHighlighting;
  }

  public void setSyntaxHighlighting(String syntaxHighlighting) {
    this.syntaxHighlighting = syntaxHighlighting;
  }

  public String getPasteExpiration() {
    return pasteExpiration;
  }

  public void setPasteExpiration(String pasteExpiration) {
    this.pasteExpiration = pasteExpiration;
  }

  public String getPasteExposure() {
    return pasteExposure;
  }

  public void setPasteExposure(String pasteExposure) {
    this.pasteExposure = pasteExposure;
  }
}
