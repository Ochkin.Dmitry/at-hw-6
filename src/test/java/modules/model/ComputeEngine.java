package modules.model;

public class ComputeEngine {

  private String numberOffInstances;
  private String instancesFor;
  private String OS;
  private String machineClass;
  private String machineType;
  private boolean isWithGPUs;
  private String numberOfGPUs;
  private String GPUType;
  private String localSSD;
  private String dataCenterLocation;
  private String committedUsage;

  public String getNumberOffInstances() {
    return numberOffInstances;
  }

  public void setNumberOffInstances(String numberOffInstances) {
    this.numberOffInstances = numberOffInstances;
  }

  public String getInstancesFor() {
    return instancesFor;
  }

  public void setInstancesFor(String instancesFor) {
    this.instancesFor = instancesFor;
  }

  public String getOS() {
    return OS;
  }

  public void setOS(String OS) {
    this.OS = OS;
  }

  public String getMachineClass() {
    return machineClass;
  }

  public void setMachineClass(String machineClass) {
    this.machineClass = machineClass;
  }

  public String getMachineType() {
    return machineType;
  }

  public void setMachineType(String machineType) {
    this.machineType = machineType;
  }

  public boolean isWithGPUs() {
    return isWithGPUs;
  }

  public void setWithGPUs(boolean withGPUs) {
    isWithGPUs = withGPUs;
  }

  public String getNumberOfGPUs() {
    return numberOfGPUs;
  }

  public void setNumberOfGPUs(String numberOfGPUs) {
    this.numberOfGPUs = numberOfGPUs;
  }

  public String getGPUType() {
    return GPUType;
  }

  public void setGPUType(String GPUType) {
    this.GPUType = GPUType;
  }

  public String getLocalSSD() {
    return localSSD;
  }

  public void setLocalSSD(String localSSD) {
    this.localSSD = localSSD;
  }

  public String getDataCenterLocation() {
    return dataCenterLocation;
  }

  public void setDataCenterLocation(String dataCenterLocation) {
    this.dataCenterLocation = dataCenterLocation;
  }

  public String getCommittedUsage() {
    return committedUsage;
  }

  public void setCommittedUsage(String committedUsage) {
    this.committedUsage = committedUsage;
  }
}
