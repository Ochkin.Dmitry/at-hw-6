package modules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractPage {

  protected WebDriver driver;

  public AbstractPage(WebDriver driver) {
    this.driver = driver;
    this.driver.manage().window().fullscreen();
  }

  protected abstract AbstractPage open();

  protected void setFieldValue(WebElement element, String value) {
    element.clear();
    element.sendKeys(value);
  }

  protected WebElement waitForElementLocated(By by) {
    //    WebDriverWait wait = new WebDriverWait(driver, 5).
    //    FluentWait wait = new FluentWait();
    return new WebDriverWait(driver, 5).until(ExpectedConditions.presenceOfElementLocated(by));
  }
}
