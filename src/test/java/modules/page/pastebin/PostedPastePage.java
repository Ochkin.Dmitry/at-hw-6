package modules.page.pastebin;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PostedPastePage extends AbstractPasteBinPage {

  public PostedPastePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @FindBy(xpath = "//div[@class='paste_box_line1']")
  private WebElement pasteTitle;

  @FindBy(id = "paste_code")
  private WebElement pasteRawCode;

  @FindBy(id = "selectable")
  private WebElement selectableCode;

  @Override
  public PostedPastePage open() {
    new PasteBinHomePage(driver).savePaste();
    return this;
  }

  public String titleName() {
    return pasteTitle.getAttribute("title");
  }

  public String pasteRawCode() {
    return pasteRawCode.getText();
  }

  public boolean isCodeBashSyntax() {
    List<WebElement> elements =
        selectableCode.findElements(By.xpath("/span[@class='kw2']"));
    return elements.size() > 0;
  }
}
