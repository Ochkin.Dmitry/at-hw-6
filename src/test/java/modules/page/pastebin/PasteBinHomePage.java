package modules.page.pastebin;

import modules.model.PasteBin;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PasteBinHomePage extends AbstractPasteBinPage {

  private static final String SELECT_BY_LABEL_LOCATOR =
      "//div[contains(text(), '%s')]/following::div[1]/select";

  @FindBy(xpath = "//input[@name='paste_name']")
  private WebElement pasteTitleField;

  @FindBy(xpath = "//input[@name='submit']")
  private WebElement createButton;

  @FindBy(xpath = "//textarea[@name='paste_code']")
  private WebElement codeField;

  @FindBy(xpath = "//div[@id='success']")
  private WebElement successMessage;

  public PasteBinHomePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @Override
  public PasteBinHomePage open() {
    driver.get("https://pastebin.com/");
    return this;
  }

  public PasteBinHomePage createNewPaste(PasteBin pasteBin) {
    setFieldValue(codeField, pasteBin.getCode());
    findSelectByLabel("Syntax Highlighting:").selectByVisibleText(pasteBin.getSyntaxHighlighting());
    findSelectByLabel("Paste Expiration:").selectByVisibleText(pasteBin.getPasteExpiration());
    findSelectByLabel("Paste Exposure:").selectByVisibleText(pasteBin.getPasteExposure());
    pasteTitleField.sendKeys(pasteBin.getPasteTitle());
    savePaste();
    return this;
  }

  private Select findSelectByLabel(String label) {
    return new Select(driver.findElement(By.xpath(String.format(SELECT_BY_LABEL_LOCATOR, label))));
  }

  public void savePaste() {
    createButton.click();
  }
}
