package modules.page.pastebin;

import modules.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

abstract class AbstractPasteBinPage extends AbstractPage {

  protected final String STATUS_LOCATOR =
      "//div[contains(@class, 'content_text')]/descendant::div[1]";

  public AbstractPasteBinPage(WebDriver driver) {
    super(driver);
  }

  protected void selectValue(WebElement element, String value) {
    new Select(element).selectByValue(value);
  }

  protected void setFieldValue(WebElement element, String value) {
    element.clear();
    element.sendKeys(value);
  }

  public boolean isSuccessStatus() {
    String status = driver.findElement(By.xpath(STATUS_LOCATOR)).getAttribute("id");
    return status.equals("success");
  }
}
