package modules.page.google_cloud;

import static utils.PageUtils.syntheticClick;

import modules.AbstractPage;
import modules.model.ComputeEngine;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ComputeEnginePage extends AbstractPage {

  public ComputeEnginePage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  public ComputeEnginePage fillComputeEngineForm(ComputeEngine product) {
    fillNumberOfInstances(product.getNumberOffInstances());
    fillInstanceFor(product.getInstancesFor());
    selectOperatingSystem(product.getOS());
    selectMachineClass(product.getMachineClass());
    selectMachineType(product.getMachineType());
    setWithGPUsValue(product.isWithGPUs());
    selectNumberOfGPUs(product.getNumberOfGPUs());
    selectGPUType(product.getGPUType());
    selectLocalSSD(product.getLocalSSD());
    selectDataCenterLocation(product.getDataCenterLocation());
    return this;
  }

  public ComputeEnginePage verifyComputeEngineEstimateForm() {
    waitForElementLocated(
        By.xpath(
            "//h2[@class='md-toolbar-tools']/child::span[contains(text(), 'Compute Engine')]"));
    return this;
  }

  public WebElement getVMClassEstimated() {
    return findListItemByTextContains("VM class: ");
  }

  public WebElement getInstanceEstimated() {
    return findListItemByTextContains("Instance type: ");
  }

  public WebElement getRegionEstimated() {
    return findListItemByTextContains("Region: ");
  }

  public WebElement getLocalSSDSpaceEstimated() {
    return findListItemByTextContains("Total available local SSD space");
  }

  public boolean isExpectedEstimateValue(WebElement element, String value) {
    return element.getText().toLowerCase().contains(value.toLowerCase());
  }

  public String getEstimateCost() {
    String fieldName = "Estimated Component Cost: ";
    String itemText =
        driver
            .findElement(
                By.xpath(
                    "//div[contains(@class, 'md-list-item-text')]//b[contains(text(), 'Estimated Component Cost:')]"))
            .getText();
    return itemText.replace(fieldName, "");
  }

  public WebElement findListItemByTextContains(String text) {
    return driver.findElement(
        By.xpath(
            String.format(
                "//div[@class='md-list-item-text ng-binding'][contains(text(), '%s')]", text)));
  }

  public void fillNumberOfInstances(String value) {
    findInputByLabel("Number of instances").sendKeys(value);
  }

  public void fillInstanceFor(String value) {
    findInputByLabel("What are these instances for?").sendKeys(value);
  }

  public void selectOperatingSystem(String value) {
    selectValue(findSelectByLabel("Operating System / Software"), value);
  }

  public void selectMachineClass(String value) {
    selectValue(findSelectByLabel("Machine Class"), value);
  }

  public void selectMachineType(String value) {
    selectValue(findSelectByLabel("Machine type"), value);
  }

  public void setWithGPUsValue(boolean value) {
    setCheckboxValue("Add GPUs.", value);
  }

  public void selectNumberOfGPUs(String value) {
    selectValue(findSelectByLabel("Number of GPUs"), value);
  }

  public void selectGPUType(String value) {
    selectValue(findSelectByLabel("GPU type"), value);
  }

  public void selectLocalSSD(String value) {
    selectValue(findSelectByLabel("Local SSD"), value);
  }

  public void selectDataCenterLocation(String value) {
    selectValue(findSelectByLabel("Datacenter location"), value);
  }

  public void selectCommittedUsage(String value) {
    selectValue(findSelectByLabel("Committed usage"), value);
  }

  public ComputeEnginePage addToEstimateInstances() {
    driver.switchTo().parentFrame();
    WebDriverWait wait = new WebDriverWait(driver, 5);
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.tagName("iframe")));
    WebElement addButton =
        driver.findElement(
            By.xpath("//h2[text()='Instances']/..//button[contains(text(), 'Add to Estimate')]"));
    syntheticClick(driver, addButton);
    return this;
  }

  private void setCheckboxValue(String label, boolean value) {
    WebElement element =
        driver.findElement(
            By.xpath(String.format("//div[contains(text(), '%s')]/parent::md-checkbox", label)));
    if (element.isSelected() != value) syntheticClick(driver, element);
  }

  public void selectValue(WebElement webElement, String value) {
    syntheticClick(driver, webElement);
    driver.switchTo().parentFrame();
    WebDriverWait wait = new WebDriverWait(driver, 5);
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.tagName("iframe")));
    String locator = "//div[contains(@class, 'md-active')]//div[contains(text(), '%s')]/..";
    WebElement element =
        waitForElementLocated(
            By.xpath(
                String.format(
                    "//div[contains(@class, 'md-active')]//div[contains(text(), '%s')]/..",
                    value)));
    syntheticClick(driver, element);
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected ComputeEnginePage open() {
    new PricingCalculatorPage(driver).selectComputeEngineSection();
    return this;
  }

  private WebElement findInputByLabel(String label) {
    return driver.findElement(
        By.xpath(String.format("//label[contains(text(), '%s')]/following-sibling::input", label)));
  }

  private WebElement findSelectByLabel(String label) {
    return driver.findElement(
        By.xpath(
            String.format(
                "//label[contains(text(), '%s')]/..//span[@class='md-select-icon']", label)));
  }
}
