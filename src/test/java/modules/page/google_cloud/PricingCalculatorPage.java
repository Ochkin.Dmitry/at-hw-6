package modules.page.google_cloud;

import modules.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.PageUtils;

public class PricingCalculatorPage extends AbstractPage {

  @FindBy(
      xpath = "//parent::div[@title='Compute Engine' and @class!='hexagon']/parent::md-tab-item")
  private WebElement computeEngineButton;

  public PricingCalculatorPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @Override
  public PricingCalculatorPage open() {
    new GoogleCloudPage(driver).openPricingCalculatorPage();
    return this;
  }

  public ComputeEnginePage selectComputeEngineSection() {
    switchToMainFrame();
    PageUtils.syntheticClick(driver, computeEngineButton);
    return new ComputeEnginePage(driver);
  }

  private void switchToMainFrame() {
    WebDriverWait wait = new WebDriverWait(driver, 5);
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.tagName("iframe")));
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.tagName("iframe")));
  }
}
