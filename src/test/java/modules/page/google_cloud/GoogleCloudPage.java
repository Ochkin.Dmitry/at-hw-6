package modules.page.google_cloud;

import modules.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utils.PageUtils;

public class GoogleCloudPage extends AbstractPage {

  public GoogleCloudPage(WebDriver driver) {
    super(driver);
    PageFactory.initElements(driver, this);
  }

  @Override
  public GoogleCloudPage open() {
    driver.get("https://cloud.google.com/");
    return this;
  }

  public PricingCalculatorPage openPricingCalculatorPage() {
    WebElement pricingTab =
        waitForElementLocated(
            By.xpath("//div[@class='devsite-tabs-wrapper']/tab[./a[contains(text(), 'Pricing')]]"));
    PageUtils.syntheticClick(driver, pricingTab);
    WebElement calculatorLink =
        waitForElementLocated(
            By.xpath("//div[@class='devsite-nav-item-title' and contains(text(), 'Calculators')]"));
    calculatorLink.click();
    return new PricingCalculatorPage(driver);
  }
}
