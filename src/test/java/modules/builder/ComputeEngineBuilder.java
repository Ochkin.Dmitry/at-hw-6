package modules.builder;

import modules.model.ComputeEngine;

public class ComputeEngineBuilder {

  public ComputeEngine buildComputeEngineForm(){
    ComputeEngine product = new ComputeEngine();
    product.setNumberOffInstances("4");
    product.setInstancesFor("");
    product.setOS("Free: Debian, CentOS, CoreOS, Ubuntu, or other User Provided OS");
    product.setMachineClass("Regular");
    product.setMachineType("n1-standard-8 (vCPUs: 8, RAM: 30GB)");
    product.setWithGPUs(true);
    product.setNumberOfGPUs("1");
    product.setGPUType("NVIDIA Tesla V100");
    product.setLocalSSD("2x375 GB");
    product.setDataCenterLocation("Frankfurt (europe-west3)");
    product.setCommittedUsage("1 Year");
    return product;
  }
}
