package modules.builder;

import modules.model.PasteBin;

public class PasteBuilder {

  public PasteBin buildPaste() {
    PasteBin paste = new PasteBin();
    paste.setCode("Hello from WebDriver");
    paste.setSyntaxHighlighting("None");
    paste.setPasteExpiration("10 Minutes");
    paste.setPasteExposure("Public");
    paste.setPasteTitle("helloweb");
    return paste;
  }

  public PasteBin buildPasteBash() {
    PasteBin paste = new PasteBin();
    paste.setCode("git config --global user.name  \"New Sheriff in Town\"\n"
       + "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n"
       + "git push origin master --force");
    paste.setSyntaxHighlighting("Bash");
    paste.setPasteExpiration("10 Minutes");
    paste.setPasteExposure("Public");
    paste.setPasteTitle("how to gain dominance among developers");
    return paste;
  }
}
